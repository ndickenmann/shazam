/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <math.h>

#include "main.h"
#include "BLE_Manager.h"
#include "OTA.h"
#include "hci.h"
#include "fann.h"


/* Private define ------------------------------------------------------------*/
#define CHECK_VIBRATION_PARAM ((uint16_t)0x1234)

#define READ_ENV 1000
#define READ_MOTION 100
#define PRINT_SENSORS 1000
#define THR_mG	1800
#define AVG_SIZE 10

#define COLLECT 0 // If 1 collect a new databases, if 0 execute the NN DONE
#define FANN_SIZE 40 // Sampled at ~4Hz in 10 sec DONE

/* The parameter fftLen specifies the length of RFFT/CIFFT process.
Supported FFT Lengths are 32, 64, 128, 256, 512, 1024, 2048, 4096. */
#define FFT_SIZE (4096)
/* SAMPLING RATE AND ADC CONFIGURATIONS ARE IN
 * STWIN_conf.h
 * - N_MS_PER_INTERRUPT: it is the period in which the ADC fulfill the input buffer, MIN 1ms
 * - AUDIO_IN_SAMPLING_FREQUENCY: ADC sampling frequency (default 16 kHz)
 * - AUDIO_IN_CHANNELS: default value always at 1
 * main.h
 * - ALGO_FREQ_AUDIO_LEVEL: Update frequency for mic audio level [Hz]
 * - ALGO_PERIOD_AUDIO_LEVEL: Update period for mic audio level [ms]
 *
 * NOTE:
 * PCM_Buffer size must be == FFT_SIZE, PCM_Buffer size = ((AUDIO_IN_CHANNELS*AUDIO_IN_SAMPLING_FREQUENCY)/1000)  * N_MS
 * see file TargetPlatform.c
 */
#define AUDIO_BUFFER_SIZE (((AUDIO_IN_CHANNELS*AUDIO_IN_SAMPLING_FREQUENCY)/1000)  * N_MS)
#if (AUDIO_BUFFER_SIZE != FFT_SIZE)
#error "PCM_Buffer size must be == FFT_SIZE. See FFT_SIZE, AUDIO_IN_SAMPLING_FREQUENCY, N_MS"
#endif

/* Imported Variables -------------------------------------------------------------*/

/* Exported Variables -------------------------------------------------------------*/
volatile uint32_t HCI_ProcessEvent=      0;
volatile uint8_t FifoEnabled = 0;

volatile uint32_t PredictiveMaintenance = 0;

float RMS_Ch[AUDIO_IN_CHANNELS];
float DBNOISE_Value_Old_Ch[AUDIO_IN_CHANNELS];

uint32_t ConnectionBleStatus  =0;

TIM_HandleTypeDef    TimCCHandle;

uint8_t EnvironmentalTimerEnabled= 0;
uint8_t AudioLevelTimerEnabled= 0;
uint8_t InertialTimerEnabled= 0;
uint8_t BatteryTimerEnabled= 0;

uint8_t AudioLevelEnable= 0;

uint32_t uhCCR1_Val = DEFAULT_uhCCR1_Val;
uint32_t uhCCR2_Val = DEFAULT_uhCCR2_Val;
uint32_t uhCCR3_Val = DEFAULT_uhCCR3_Val;
uint32_t uhCCR4_Val = DEFAULT_uhCCR4_Val;

uint8_t  NodeName[8];

/* Private variables ---------------------------------------------------------*/
uint16_t VibrationParam[11];

MOTION_SENSOR_Axes_t ACC_Value[AVG_SIZE];
MOTION_SENSOR_Axes_t GYR_Value[AVG_SIZE];
MOTION_SENSOR_Axes_t MAG_Value[AVG_SIZE];
/* pointer */
uint8_t pp;
/* env sensors */
float pres;
float hum;
float temp;
float temp2;
/* variables from the Audio Driver */
extern uint16_t PCM_Buffer[];
uint16_t DBNOISE_Value_Ch[AUDIO_IN_CHANNELS];
arm_rfft_fast_instance_f32 rfft_inst_f32;
float maxvalue;
uint32_t maxindex;
float freq11;
float freq12;
float freq13;
float freq21;
float freq22;
float freq23;
float freq31;
float freq32;
float freq33;
uint16_t FFTready = 0;

///* Dimensioned considered a 1-axial FFT */   // DONE
fann_type acc[FANN_SIZE*9];
fann_type *output;

/* Table with All the known Meta Data */
MDM_knownGMD_t known_MetaData[]={
		{GMD_NODE_NAME,      (sizeof(NodeName))},
		{GMD_VIBRATION_PARAM,(sizeof(VibrationParam))},
		{GMD_END    ,0}/* THIS MUST BE THE LAST ONE */
};

static volatile uint32_t ButtonPressed=         0;
static volatile uint32_t SendEnv=               0;
static volatile uint32_t SendAudioLevel=        0;
static volatile uint32_t SendAccGyroMag=        0;
static volatile uint32_t SendBatteryInfo=       0;
static volatile uint32_t t_stwin=               0;

uint32_t NumSample= ((AUDIO_IN_CHANNELS*AUDIO_IN_SAMPLING_FREQUENCY)/1000)  * N_MS;

/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void InitPredictiveMaintenance(void);
static unsigned char ReCallVibrationParamFromMemory(void);
static void EnvironmentalData(void);
static void MotionData(void);
static void BatteryInfoData(void);
static void ButtonCallback(void);
static void BatteryFeatures_Start(void);
static void Init_Audio(void);
static void AudioLevelData(void);
static void AudioProcess_DB_Noise(void);
static void AudioProcess_FFT(void);
static void AudioProcess(void);
static MOTION_SENSOR_Axes_t MotionAvg (MOTION_SENSOR_Axes_t *in);
static void MinMaxNormalization(fann_type *in, fann_type *out, int len);
static void ToggleTheLED(Led_TypeDef LED, uint32_t ticks);


/**
 * @brief  Main program
 * @param  None
 * @retval None
 */
int main(void)
{

	uint32_t timeenv=0;
	uint32_t timemotion=0;
	uint32_t timeprint=0;
	MOTION_SENSOR_Axes_t tempA;
	int out;

	HAL_Init();

	HAL_PWREx_EnableVddIO2();
	__HAL_RCC_PWR_CLK_ENABLE();
	HAL_PWREx_EnableVddUSB();

	/* Configure the System clock */
	SystemClock_Config();

	/* Hello from Illi
	 * This function configures all the sensors
	 * and the USB connection.
	 *
	 * Sensor configurations and init are
	 * in Core/Driver/BSP/Components
	 * hts221
	 * iis2mdc
	 * ism330dhcx
	 * lps22hh
	 *
	 * To change the acquisition and or
	 * elaboration period see:
	 * main.h
	 * - FREQ_ACC_GYRO_MAG
	 * - ALGO_FREQ_ENV
	 * - ALGO_FREQ_AUDIO_LEVEL
	 *
	 * Audio in Core/Driver/BSP/STWIN
	 *
	 * Sampling rate and window
	 * settings see:
	 * STWIN_conf.h
	 * - AUDIO_IN_SAMPLING_FREQUENCY
	 * - N_MS
	 */
	InitTargetPlatform();

	t_stwin = HAL_GetTick();

	/* Check the MetaDataManager */
	InitMetaDataManager((void *)&known_MetaData,MDM_DATA_TYPE_GMD,NULL);

	PREDMNT1_PRINTF("\n\t(HAL %ld.%ld.%ld_%ld)\r\n"
			"\tCompiled %s %s"
			" (STM32CubeIDE)\r\n"
			"\tSend Every %4dmS Temperature/Humidity/Pressure\r\n"
			"\tSend Every %4dmS Acc/Gyro/Magneto\r\n"
			"\tSend Every %4dmS dB noise\r\n\n",
			HAL_GetHalVersion() >>24,
			(HAL_GetHalVersion() >>16)&0xFF,
			(HAL_GetHalVersion() >> 8)&0xFF,
			HAL_GetHalVersion()      &0xFF,
			__DATE__,__TIME__,
			ALGO_PERIOD_ENV,
			ALGO_PERIOD_ACC_GYRO_MAG,
			ALGO_PERIOD_AUDIO_LEVEL);

#ifdef PREDMNT1_DEBUG_CONNECTION
	PREDMNT1_PRINTF("Debug Connection         Enabled\r\n");
#endif /* PREDMNT1_DEBUG_CONNECTION */

	/* Predictive Maintenance Initialization */
	InitPredictiveMaintenance();

	/* Init battery charging and monitoring */
	BatteryFeatures_Start();

	/* Init Audio Sampling */
	Init_Audio();

	/* Infinite loop */
	while (1)
	{


#if COLLECT
		/*******************************/
		/******** do not change ********/

		if(ButtonPressed == 1){
			BSP_LED_On(LED1);

			/* Sampled at ~4Hz in 10 sec DONE*/
			for(int i = 0; i < FANN_SIZE; i++){
				while(FFTready == 0){}
				PREDMNT1_PRINTF(" %d,", (int)freq11);
				PREDMNT1_PRINTF(" %d,", (int)freq12);
				PREDMNT1_PRINTF(" %d,", (int)freq13);
				PREDMNT1_PRINTF(" %d,", (int)freq21);
				PREDMNT1_PRINTF(" %d,", (int)freq22);
				PREDMNT1_PRINTF(" %d,", (int)freq23);
				PREDMNT1_PRINTF(" %d,", (int)freq31);
				PREDMNT1_PRINTF(" %d,", (int)freq32);
				PREDMNT1_PRINTF(" %d,", (int)freq33);
				FFTready = 0;

			}
			PREDMNT1_PRINTF(" \n");

			ButtonPressed = 0;

			BSP_LED_Off(LED1);

		}

		/*******************************/
		/*******************************/
#else
		/*******************************/
		/**** change here for LAB7 *****/

		if(ButtonPressed == 1){
			BSP_LED_On(LED1);

			/* sample at 4 Hz in 10s */
			int k = 0;
			for(int i = 0; i < FANN_SIZE; i++){
				//AudioLevelData();
				while(FFTready == 0){}
				acc[k++] = (fann_type)freq11;
				acc[k++] = (fann_type)freq12;
				acc[k++] = (fann_type)freq13;
				acc[k++] = (fann_type)freq21;
				acc[k++] = (fann_type)freq22;
				acc[k++] = (fann_type)freq23;
				acc[k++] = (fann_type)freq31;
				acc[k++] = (fann_type)freq32;
				acc[k++] = (fann_type)freq33;
				FFTready = 0;
			}

			MinMaxNormalization(acc, acc, FANN_SIZE*9);
			output = fann_run(acc);

			BSP_LED_Off(LED1);

			out = (int)(output[0]*100);
			PREDMNT1_PRINTF("Billie: %d %% \n\r", out);
			out = (int)(output[1]*100);
			PREDMNT1_PRINTF("Midnight: %d %% \n\r", out);
			out = (int)(output[2]*100);
			PREDMNT1_PRINTF("Walk: %d %% \n\r\n\r", out);


			if(output[0]>0.9){
				ToggleTheLED(LED2, 100);
			}else if(output[1]>0.9){
				ToggleTheLED(LED2, 100);
				ToggleTheLED(LED2, 100);
			}else if(output[2]>0.9){
				ToggleTheLED(LED2, 100);
				ToggleTheLED(LED2, 100);
				ToggleTheLED(LED2, 100);

			}

			ButtonPressed = 0;



		}

		/*******************************/
		/*******************************/
#endif

	}
}

/**
 * @brief  Callback for user button
 * @param  None
 * @retval None
 */
static void ButtonCallback(void)
{
	PREDMNT1_PRINTF("\r\nUser Button Pressed\r\n\r\n");
}

/**
 * @brief  Call this function to configure the Audio microphone Analog to Digital Conversion
 * @param None
 * @retval None
 */
static void Init_Audio (void)
{

	int32_t Count;

    InitMics(AUDIO_IN_SAMPLING_FREQUENCY, AUDIO_VOLUME_INPUT);
    AudioLevelEnable= 1;

    for(Count=0;Count<AUDIO_IN_CHANNELS;Count++) {
      RMS_Ch[Count]=0;
      DBNOISE_Value_Old_Ch[Count] =0;
    }

	arm_rfft_fast_init_f32	(&rfft_inst_f32, FFT_SIZE);

	PREDMNT1_PRINTF("--->dB Noise AudioLevel=%s"," ON\r\n");

}

/**
  * @brief  Send Audio Level Data (Ch1) to BLE
  * @param  None
  * @retval None
  */
static void AudioLevelData(void)
{

  int32_t NumberMic;


  for(NumberMic=0;NumberMic<(AUDIO_IN_CHANNELS);NumberMic++) {
    DBNOISE_Value_Ch[NumberMic] = 0;

    RMS_Ch[NumberMic] /= ((float)(NumSample/AUDIO_IN_CHANNELS)*ALGO_PERIOD_AUDIO_LEVEL);

    DBNOISE_Value_Ch[NumberMic] = (uint16_t)((120.0f - 20 * log10f(32768 * (1 + 0.25f * (AUDIO_VOLUME_INPUT /*AudioInVolume*/ - 4))) + 10.0f * log10f(RMS_Ch[NumberMic])) * 0.3f + DBNOISE_Value_Old_Ch[NumberMic] * 0.7f);
    DBNOISE_Value_Old_Ch[NumberMic] = DBNOISE_Value_Ch[NumberMic];
    RMS_Ch[NumberMic] = 0.0f;
  }

	/* Leds off if the noise level is below 35 dB */
//	if (DBNOISE_Value_Ch[0] < 35){
//		BSP_LED_Off(LED1);
//		BSP_LED_Off(LED2);
//	}

}

/**
* @brief  User function that is called when PDM data is available.
* @param  none
* @retval None
*/
static void AudioProcess(void)
{
  if(AudioLevelEnable)
  {
	  AudioProcess_FFT();
	  FFTready = 1;
  }
}

/**
* @brief  User function that is called when PDM data is available.
* @param  none
* @retval None
*/
static void AudioProcess_DB_Noise(void)
{
  int32_t i;
  int32_t NumberMic;

  if(AudioLevelEnable) {
    for(i = 0; i < (NumSample/2); i++){
      for(NumberMic=0;NumberMic<AUDIO_IN_CHANNELS;NumberMic++) {
        RMS_Ch[NumberMic] += (float)((int16_t)PCM_Buffer[i*AUDIO_IN_CHANNELS+NumberMic] * ((int16_t)PCM_Buffer[i*AUDIO_IN_CHANNELS+NumberMic]));
      }
    }
  }
}

/* Reference: "Digital Filters and Signal Processing" 2nd Ed.
 * by L. B. Jackson. (1989) Kluwer Academic Publishers.
 * ISBN 0-89838-276-9
 * Sec.7.3 - Windows in Spectrum Analysis
 */
float hanning (int i, int nn)
{
  return (float)( 0.5 * (1.0 - cos (2.0*M_PI*(double)i/(double)(nn-1))) );
}

void windowing (float *Buffer, int size)
{
	int i;

	for(i=0; i<size; i++){

		Buffer[i] = Buffer[i] * hanning (i, size);

	}

}

/**
 * @brief  User function that is called when the PDM data is available.
 * It performs an FFT transformation.
 * @param  none
 * @retval None
 */
static void AudioProcess_FFT(void)
{

	float FFT_Buffer[FFT_SIZE];
	float IN_Buffer[FFT_SIZE];
	int16_t convert;

	for(int i=0; i < FFT_SIZE;i++){
		convert = (int16_t)PCM_Buffer[i];
		IN_Buffer[i] = (float)convert;
	}

	/* apply Hanning window */
	windowing (IN_Buffer, FFT_SIZE);

	/* FFT */
	arm_rfft_fast_f32(&rfft_inst_f32, IN_Buffer, FFT_Buffer, 0);
	/* calc resolution fft */
	float res = (((float)(AUDIO_IN_SAMPLING_FREQUENCY))/((float)(FFT_SIZE)));

	/* abs value */
	arm_cmplx_mag_f32(FFT_Buffer, FFT_Buffer, FFT_SIZE/2);


//first third
	/* find the maximum, FFT_Buffer[0] is the DC component */
	arm_max_f32(&FFT_Buffer[1], (FFT_SIZE/6) - 1, &maxvalue, &maxindex);
	/* fix the index adding the DC component */
	maxindex++;

	/* estimate the frequency using the FTT resultion and the maximum
	 * value (max sound energy on a specific frequency)
	 */
	freq11 = maxindex * res;

	FFT_Buffer[maxindex] = 0;
	arm_max_f32(&FFT_Buffer[1], (FFT_SIZE/6) - 1, &maxvalue, &maxindex);
	maxindex++;
	freq12 = maxindex * res;

	FFT_Buffer[maxindex] = 0;
	arm_max_f32(&FFT_Buffer[1], (FFT_SIZE/6) - 1, &maxvalue, &maxindex);
	maxindex++;
	freq13 = maxindex * res;


//second third
	FFT_Buffer[maxindex] = 0;
	arm_max_f32(&FFT_Buffer[(FFT_SIZE/6)], (FFT_SIZE/6) - 1, &maxvalue, &maxindex);
	maxindex += (FFT_SIZE/6);
	freq21 = maxindex * res;

	FFT_Buffer[maxindex] = 0;
	arm_max_f32(&FFT_Buffer[(FFT_SIZE/6)], (FFT_SIZE/6) - 1, &maxvalue, &maxindex);
	maxindex += (FFT_SIZE/6);
	freq22 = maxindex * res;

	FFT_Buffer[maxindex] = 0;
	arm_max_f32(&FFT_Buffer[(FFT_SIZE/6)], (FFT_SIZE/6) - 1, &maxvalue, &maxindex);
	maxindex += (FFT_SIZE/6);
	freq23 = maxindex * res;

//third third of FFT
	FFT_Buffer[maxindex] = 0;
	arm_max_f32(&FFT_Buffer[2 * (FFT_SIZE/6)], (FFT_SIZE/6) - 1, &maxvalue, &maxindex);
	maxindex += 2 * (FFT_SIZE/6);
	freq31 = maxindex * res;

	FFT_Buffer[maxindex] = 0;
	arm_max_f32(&FFT_Buffer[2 * (FFT_SIZE/6)], (FFT_SIZE/6) - 1, &maxvalue, &maxindex);
	maxindex += 2 * (FFT_SIZE/6);
	freq32 = maxindex * res;

	FFT_Buffer[maxindex] = 0;
	arm_max_f32(&FFT_Buffer[2 * (FFT_SIZE/6)], (FFT_SIZE/6) - 1, &maxvalue, &maxindex);
	maxindex += 2 * (FFT_SIZE/6);
	freq33 = maxindex * res;



}

static MOTION_SENSOR_Axes_t MotionAvg (MOTION_SENSOR_Axes_t *in){

	MOTION_SENSOR_Axes_t a = {0};

	for(int i=0;i<AVG_SIZE;i++){
		a.x += in[i].x;
		a.y += in[i].y;
		a.z += in[i].z;
	}

	a.x /= AVG_SIZE;
	a.y /= AVG_SIZE;
	a.z /= AVG_SIZE;

	return a;

}

MOTION_SENSOR_Axes_t mag_min = {10000,10000,10000}, mag_max = {-10000,-10000,-10000};
void MagCalibration ( void ){

	/* find min */
	if (MAG_Value[pp].x < mag_min.x) mag_min.x = MAG_Value[pp].x;
	if (MAG_Value[pp].y < mag_min.y) mag_min.y = MAG_Value[pp].y;
	if (MAG_Value[pp].z < mag_min.z) mag_min.z = MAG_Value[pp].z;
	/* find max */
	if (MAG_Value[pp].x > mag_max.x) mag_max.x = MAG_Value[pp].x;
	if (MAG_Value[pp].y > mag_max.y) mag_max.y = MAG_Value[pp].y;
	if (MAG_Value[pp].z > mag_max.z) mag_max.z = MAG_Value[pp].z;
	/* hard-iron calibration */
	MAG_Value[pp].x -= (mag_max.x + mag_min.x)/2;
	MAG_Value[pp].y -= (mag_max.y + mag_min.y)/2;
	MAG_Value[pp].z -= (mag_max.z + mag_min.z)/2;

}

/**
 * @brief  Send Motion Data Acc/Mag/Gyro to BLE
 * @param  None
 * @retval None
 */
static void MotionData(void)
{

	/* Read the Acc values */
	if(TargetBoardFeatures.AccSensorIsInit)
	{
		MOTION_SENSOR_GetAxes(ACCELERO_INSTANCE, MOTION_ACCELERO, &ACC_Value[pp]);
	}

	/* Read the Gyro values */
	if(TargetBoardFeatures.GyroSensorIsInit)
	{
		MOTION_SENSOR_GetAxes(GYRO_INSTANCE,MOTION_GYRO, &GYR_Value[pp]);
	}

	/* Read the Magneto values */
	if(TargetBoardFeatures.MagSensorIsInit)
	{
		MOTION_SENSOR_GetAxes(MAGNETO_INSTANCE, MOTION_MAGNETO, &MAG_Value[pp]);
	}

	/* check a blow */
	/* check the absolute value of each eaxes, and the it is compared with
	 * the threshold
	 */
	if ((abs(ACC_Value[pp].x) > THR_mG) || (abs(ACC_Value[pp].y) > THR_mG) ||
			(abs(ACC_Value[pp].z) > THR_mG)){
		/* Read sensors */
		PREDMNT1_PRINTF("/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!/\n\r");
		PREDMNT1_PRINTF("\n\r");
		PREDMNT1_PRINTF("ALARM!\n\r");
		PREDMNT1_PRINTF("\n\r");
		PREDMNT1_PRINTF("Acc mg >   x: %4d, y: %4d, z: %4d \n\r",
				(int)ACC_Value[pp].x,(int)ACC_Value[pp].y,(int)ACC_Value[pp].z);
		PREDMNT1_PRINTF("\n\r");
		PREDMNT1_PRINTF("/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!/\n\r");
		PREDMNT1_PRINTF("\n\r");
	}

	/* hard iron calibration */
	MagCalibration ();

	/* increment the pointer to the next array location */
	pp++;
	if (pp == AVG_SIZE){
		pp = 0;
	}

}

/**
 * @brief  Read Environmental Data (Temperature/Pressure/Humidity) from sensor
 * @retval None
 */
void EnvironmentalData( void )
{

	/* Read Humidity */
	if(TargetBoardFeatures.HumSensorIsInit) {
		ENV_SENSOR_GetValue(HUMIDITY_INSTANCE,ENV_HUMIDITY,&hum);
	}

	/* Read Temperature for sensor 1 */
	if(TargetBoardFeatures.TempSensorsIsInit[0]){
		ENV_SENSOR_GetValue(TEMPERATURE_INSTANCE_1,ENV_TEMPERATURE,&temp);
	}

	/* Read Pressure */
	if(TargetBoardFeatures.PressSensorIsInit){
		ENV_SENSOR_GetValue(PRESSURE_INSTANCE,ENV_PRESSURE,&pres);
	}

	/* Read Temperature for sensor 2 */
	if(TargetBoardFeatures.TempSensorsIsInit[1]) {
		ENV_SENSOR_GetValue(TEMPERATURE_INSTANCE_2,ENV_TEMPERATURE,&temp2);
	}

}

/**
 * @brief  Send Battery Info Data (Voltage/Current/Soc) to BLE
 * @param  None
 * @retval None
 */
static void BatteryInfoData(void)
{

	stbc02_State_TypeDef BC_State = {(stbc02_ChgState_TypeDef)0, ""};
	uint32_t BatteryLevel= 0;
	uint32_t Voltage;

	/* Read the voltage value and battery level status */
	BSP_BC_GetVoltageAndLevel(&Voltage,&BatteryLevel);
	BSP_BC_GetState(&BC_State);

	//Status = 0x00; /* Low Battery */
	//Status = 0x01; /* Discharging */
	//Status = 0x02; /* End of Charging == Plugged not Charging */
	//Status = 0x03; /* Charging */
	///* All the Remaing Battery Status */
	//Status = 0x04; /* Unknown */

	/* Battery Informations */
	PREDMNT1_PRINTF("Charge= %ld%% Voltage=%ld mV BC_State= %d \n\r", BatteryLevel, Voltage, BC_State.Id);
}






/*************************************************************************************************************/
/**** Private Functions *****/
/**
 * Find max and min
 * @param  Pointer to the array that needs to be normalized
 * @param  Pointer to the max and min
 */
static void sumofarray(fann_type *in, fann_type *max, fann_type *min, int len)
{
	int i;
	*min=*max=in[0];
	for(i=1; i<len; i++)
	{
		if(*min>in[i])
			*min=in[i];
		if(*max<in[i])
			*max=in[i];
	}

}

/**
 * Min max normalization between 0 and 1
 * @param  Pointer to the array that needs to be normalized
 * @param  Pointer to the float array that will include the normalized data
 */
static void MinMaxNormalization(fann_type *in, fann_type *out, int len){

	fann_type a = -1, b = 1;
	fann_type max, min;

	sumofarray(in, &max, &min, len);

	for(int i=0; i<len; i++)
	{
		fann_type X_std = (in[i] - min) / (max - min);
		out[i] = X_std * (b-a) + a;
	}

}


static void ToggleTheLED(Led_TypeDef LED, uint32_t ticks){
	BSP_LED_Toggle(LED);
	HAL_Delay(ticks);
	BSP_LED_Toggle(LED);
	HAL_Delay(ticks);
}

































/**
 * @brief  This function is called when there is a change on the gatt attribute for Battery Features
 *         for Start/Stop Timer
 * @param  None
 * @retval None
 */
static void BatteryFeatures_Start(void)
{

    BSP_BC_CmdSend(BATMS_ON);

    BatteryTimerEnabled= 1;

    BSP_BC_CmdSend(AUTORECH_ON);


}

/**
 * @brief  Get hardware and firmware version
 *
 * @param  Hardware version
 * @param  Firmware version
 * @retval Status
 */
uint8_t getBlueNRG2_Version(uint8_t *hwVersion, uint16_t *fwVersion)
{
	uint8_t status;
	uint8_t hci_version, lmp_pal_version;
	uint16_t hci_revision, manufacturer_name, lmp_pal_subversion;
	uint8_t DTM_version_major, DTM_version_minor, DTM_version_patch, DTM_variant, BTLE_Stack_version_major, BTLE_Stack_version_minor, BTLE_Stack_version_patch, BTLE_Stack_development;
	uint16_t DTM_Build_Number, BTLE_Stack_variant, BTLE_Stack_Build_Number;


	status = hci_read_local_version_information(&hci_version, &hci_revision, &lmp_pal_version,
			&manufacturer_name, &lmp_pal_subversion);

	if (status == BLE_STATUS_SUCCESS) {
		*hwVersion = hci_revision >> 8;
	}
	else {
		PREDMNT1_PRINTF("Error= %x \r\n", status);
	}


	status = aci_hal_get_firmware_details(&DTM_version_major,
			&DTM_version_minor,
			&DTM_version_patch,
			&DTM_variant,
			&DTM_Build_Number,
			&BTLE_Stack_version_major,
			&BTLE_Stack_version_minor,
			&BTLE_Stack_version_patch,
			&BTLE_Stack_development,
			&BTLE_Stack_variant,
			&BTLE_Stack_Build_Number);

	if (status == BLE_STATUS_SUCCESS) {
		*fwVersion = BTLE_Stack_version_major  << 8;  // Major Version Number
		*fwVersion |= BTLE_Stack_version_minor << 4;  // Minor Version Number
		*fwVersion |= BTLE_Stack_version_patch;       // Patch Version Number
	}
	else {
		PREDMNT1_PRINTF("Error= %x \r\n", status);
	}


	return status;
}

/** @brief Predictive Maintenance Initialization
 * @param None
 * @retval None
 */
static void InitPredictiveMaintenance(void)
{
	/* Set the vibration parameters with default values */
	MotionSP_SetDefaultVibrationParam();

	/* Read Vibration Parameters From Memory */
	ReCallVibrationParamFromMemory();

	PREDMNT1_PRINTF("\r\nAccelerometer parameters:\r\n");
	PREDMNT1_PRINTF("AccOdr= %d\t", AcceleroParams.AccOdr);
	PREDMNT1_PRINTF("AccFifoBdr= %d\t", AcceleroParams.AccFifoBdr);
	PREDMNT1_PRINTF("fs= %d\t", AcceleroParams.fs);
	PREDMNT1_PRINTF("\r\n");

	PREDMNT1_PRINTF("\r\nMotionSP parameters:\r\n");
	PREDMNT1_PRINTF("size= %d\t", MotionSP_Parameters.FftSize);
	PREDMNT1_PRINTF("wind= %d\t", MotionSP_Parameters.window);
	PREDMNT1_PRINTF("tacq= %d\t", MotionSP_Parameters.tacq);
	PREDMNT1_PRINTF("ovl= %d\t", MotionSP_Parameters.FftOvl);
	PREDMNT1_PRINTF("subrange_num= %d\t", MotionSP_Parameters.subrange_num);
	PREDMNT1_PRINTF("\r\n\n");

	PREDMNT1_PRINTF("************************************************************************\r\n\r\n");

	/* Initializes accelerometer with vibration parameters values */
	if(MotionSP_AcceleroConfig()) {
		PREDMNT1_PRINTF("\tFailed Set Accelerometer Parameters\r\n\n");
	} else {
		PREDMNT1_PRINTF("\tOK Set Accelerometer Parameters\r\n\n");
	}
}

/**
 * @brief  System Clock Configuration
 *         The system Clock is configured as follow :
 *            System Clock source            = PLL (HSI)
 *            SYSCLK(Hz)                     =
 *            HCLK(Hz)                       =
 *            AHB Prescaler                  = 1
 *            APB1 Prescaler                 = 1
 *            APB2 Prescaler                 = 1
 *            HSE Frequency(Hz)              =
 *            PLL_M                          = 2
 *            PLL_N                          = 30
 *            PLL_P                          = 2
 *            PLL_Q                          = 2
 *            PLL_R                          = 2
 *            VDD(V)                         = 3.3
 *            Main regulator output voltage  = Scale1 mode
 *            Flash Latency(WS)              = 5
 * @param  None
 * @retval None
 */
void SystemClock_Config(void)
{
	//   __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
	//
	//  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
	//  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	//  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	/* Configure the main internal regulator output voltage */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1_BOOST) != HAL_OK)
	{
		Error_Handler();
	}

	/* Initializes the CPU, AHB and APB busses clocks */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 2;
	RCC_OscInitStruct.PLL.PLLN = 30;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV5; //RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/* Initializes the CPU, AHB and APB busses clocks */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
	{
		Error_Handler();
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC
			|RCC_PERIPHCLK_DFSDM1
			|RCC_PERIPHCLK_USB
			|RCC_PERIPHCLK_ADC
			|RCC_PERIPHCLK_I2C2
			|RCC_PERIPHCLK_SAI1;
	PeriphClkInit.Sai1ClockSelection = RCC_SAI1CLKSOURCE_PLLSAI1;
	PeriphClkInit.I2c2ClockSelection = RCC_I2C2CLKSOURCE_PCLK1;
	PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
	PeriphClkInit.Dfsdm1ClockSelection = RCC_DFSDM1CLKSOURCE_PCLK2; //RCC_DFSDM1CLKSOURCE_PCLK;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
	PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
	PeriphClkInit.PLLSAI1.PLLSAI1M = 5;
	PeriphClkInit.PLLSAI1.PLLSAI1N = 96;
	PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV25;
	PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV4;
	PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV4;
	PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK|RCC_PLLSAI1_SAI1CLK;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		Error_Handler();
	}

	/* Configure the Systick interrupt time */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

	/* Configure the Systick */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/**
 * @brief  Check if there are a valid Vibration Parameters Values in Memory and read them
 * @param pAccelerometer_Parameters Pointer to Accelerometer parameter structure
 * @param pMotionSP_Parameters Pointer to Board parameter structure
 * @retval unsigned char Success/Not Success
 */
static unsigned char ReCallVibrationParamFromMemory(void)
{
	/* ReLoad the Vibration Parameters Values from RAM */
	unsigned char Success=0;

	PREDMNT1_PRINTF("Recall the vibration parameter values from FLASH\r\n");

	/* Recall the Vibration Parameters Values saved */
	MDM_ReCallGMD(GMD_VIBRATION_PARAM,(void *)VibrationParam);

	if(VibrationParam[0] == CHECK_VIBRATION_PARAM)
	{
		AcceleroParams.AccOdr=              VibrationParam[1];
		AcceleroParams.AccFifoBdr=          VibrationParam[2];
		AcceleroParams.fs=                  VibrationParam[3];
		MotionSP_Parameters.FftSize=        VibrationParam[4];
		MotionSP_Parameters.tau=            VibrationParam[5];
		MotionSP_Parameters.window=         VibrationParam[6];
		MotionSP_Parameters.td_type=        VibrationParam[7];
		MotionSP_Parameters.tacq=           VibrationParam[8];
		MotionSP_Parameters.FftOvl=         VibrationParam[9];
		MotionSP_Parameters.subrange_num=   VibrationParam[10];

		PREDMNT1_PRINTF("Vibration parameter values read from FLASH\r\n");

		NecessityToSaveMetaDataManager=0;
	}
	else
	{
		PREDMNT1_PRINTF("Vibration parameters values not present in FLASH\r\n");
		SaveVibrationParamToMemory();
	}

	return Success;
}

/**
 * @brief  Period elapsed callback in non blocking mode for Environmental timer
 * @param  htim : TIM handle
 * @retval None
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if (htim->Instance == STBC02_USED_TIM) {
		BC_CmdMng();
#ifdef PREDMNT1_ENABLE_PRINTF
	} else if(htim == (&TimHandle)) {
		CDC_TIM_PeriodElapsedCallback(htim);
#endif /* PREDMNT1_ENABLE_PRINTF */
	}
}

/**
 * @brief  Conversion complete callback in non blocking mode
 * @param  htim : hadc handle
 * @retval None
 */
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
	if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_3)
	{
		BSP_BC_ChgPinHasToggled();
	}
}

/**
 * @brief  Half Transfer user callback, called by BSP functions.
 * @param  None
 * @retval None
 */
void BSP_AUDIO_IN_HalfTransfer_CallBack(uint32_t Instance)
{
	AudioProcess();
}

/**
 * @brief  Transfer Complete user callback, called by BSP functions.
 * @param  None
 * @retval None
 */
void BSP_AUDIO_IN_TransferComplete_CallBack(uint32_t Instance)
{
	AudioProcess();
	SendAudioLevel = 1;
}

/**
 * @brief  EXTI line detection callback.
 * @param  uint16_t GPIO_Pin Specifies the pins connected EXTI line
 * @retval None
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{  
	switch(GPIO_Pin){
	//    case HCI_TL_SPI_EXTI_PIN:
	//      hci_tl_lowlevel_isr();
	//      HCI_ProcessEvent=1;
	//    break;
	case M_INT2_O_PIN:
		if(FifoEnabled)
			MotionSP_FifoFull_IRQ_Rtn();
		else
			MotionSP_DataReady_IRQ_Rtn();
		break;

	case USER_BUTTON_PIN:
		ButtonPressed = 1;
		break;

	case GPIO_PIN_10:
		if(HAL_GetTick() - t_stwin > 4000)
		{
			BSP_BC_CmdSend(SHIPPING_MODE_ON);
		}
		break;
	}
}

/**
 * @}
 */

/** @defgroup PREDCTIVE_MAINTENANCE_MAIN_EXPORTED_FUNCTIONS Predictive Maintenance Main Exported Functions
 * @{
 */

/**
 * @brief This function provides accurate delay (in milliseconds) based
 *        on variable incremented.
 * @note This is a user implementation using WFI state
 * @param Delay: specifies the delay time length, in milliseconds.
 * @retval None
 */
void HAL_Delay(__IO uint32_t Delay)
{
	uint32_t tickstart = 0;
	tickstart = HAL_GetTick();
	while((HAL_GetTick() - tickstart) < Delay){
		__WFI();
	}
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
void Error_Handler(void)
{
	/* User may add here some code to deal with this error */
	while(1){
	}
}

/**
 * @brief  Save vibration parameters values to memory
 * @param pAccelerometer_Parameters Pointer to Accelerometer parameter structure
 * @param pMotionSP_Parameters Pointer to Board parameter structure
 * @retval unsigned char Success/Not Success
 */
unsigned char SaveVibrationParamToMemory(void)
{
	/* ReLoad the Vibration Parameters Values from RAM */
	unsigned char Success=0;

	VibrationParam[0]= CHECK_VIBRATION_PARAM;
	VibrationParam[1]=  (uint16_t)AcceleroParams.AccOdr;
	VibrationParam[2]=  (uint16_t)AcceleroParams.AccFifoBdr;
	VibrationParam[3]=  (uint16_t)AcceleroParams.fs;
	VibrationParam[4]=  (uint16_t)MotionSP_Parameters.FftSize;
	VibrationParam[5]=  (uint16_t)MotionSP_Parameters.tau;
	VibrationParam[6]=  (uint16_t)MotionSP_Parameters.window;
	VibrationParam[7]=  (uint16_t)MotionSP_Parameters.td_type;
	VibrationParam[8]=  (uint16_t)MotionSP_Parameters.tacq;
	VibrationParam[9]=  (uint16_t)MotionSP_Parameters.FftOvl;
	VibrationParam[10]= (uint16_t)MotionSP_Parameters.subrange_num;

	PREDMNT1_PRINTF("Vibration parameters values will be saved in FLASH\r\n");
	MDM_SaveGMD(GMD_VIBRATION_PARAM,(void *)VibrationParam);
	NecessityToSaveMetaDataManager=1;

	return Success;
}

/**
 * @}
 */

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{ 
	/* User can add his own implementation to report the file name and line number,
     ex: PREDMNT1_PRINTF("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1){
	}
}
#endif
