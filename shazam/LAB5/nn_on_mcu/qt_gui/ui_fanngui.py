from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
                            QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
                           QFontDatabase, QIcon, QKeySequence, QLinearGradient,
                           QPalette, QPainter, QPixmap, QRadialGradient)
from PySide2.QtWidgets import *
from qt_gui.sklearn_gui import setup_sklearn_gui


class Ui_MainWindow(object):
    def __init__(self, MainWindow):
        self.setupUi(MainWindow)

    def setupUi(self, MainWindow):
        # Set Window size and name
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.setEnabled(True)
        MainWindow.resize(1055, 745)

        self.actionNew = QAction(MainWindow)
        self.actionNew.setObjectName(u"actionNew")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.central_widget_vertical_lyt = QVBoxLayout(self.centralwidget)
        self.central_widget_vertical_lyt.setObjectName(u"verticalLayout_2")

        self.set_path_data_box()

        self.central_layout = QHBoxLayout()
        self.central_layout.setObjectName(u"central_layout")
        self.outputText = QTextBrowser(self.centralwidget)
        self.outputText.setObjectName(u"outputText")
        self.outputText.setMaximumSize(QSize(400, 16777215))

        self.central_layout.addWidget(self.outputText)

        self.library_pages = QStackedWidget(self.centralwidget)
        self.library_pages.setObjectName(u"library_pages")
        self.library_pages.setEnabled(True)
        self.library_pages.setLayoutDirection(Qt.LeftToRight)

        setup_sklearn_gui(self)
        self.TF = QWidget()
        self.TF.setObjectName(u"TF")
        self.ToDo = QLabel(self.TF)
        self.ToDo.setObjectName(u"ToDo")
        self.ToDo.setGeometry(QRect(220, 220, 71, 23))
        self.library_pages.addWidget(self.TF)

        self.central_layout.addWidget(self.library_pages)

        self.central_widget_vertical_lyt.addLayout(self.central_layout)

        # Call function to create execute buttons
        self.set_execute_btn_box()

        self.set_menu_and_tool_bar(MainWindow)

        self.retranslateUi(MainWindow)

        self.library_pages.setCurrentIndex(0)
        self.sklearn_settings_widget.setCurrentIndex(0)

        QMetaObject.connectSlotsByName(MainWindow)

    # setupUi

    def set_menu_and_tool_bar(self, MainWindow):
        # Tool and statusbar
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1055, 35))
        self.menuFile = QMenu(self.menubar)
        self.menuFile.setObjectName(u"menuFile")
        self.menuEdit = QMenu(self.menubar)
        self.menuEdit.setObjectName(u"menuEdit")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuEdit.menuAction())
        self.menuFile.addAction(self.actionNew)

    def set_path_data_box(self):
        # Input path train and validation data
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.input_path = QVBoxLayout()
        self.input_path.setObjectName(u"input_path")
        self.path_train_data = QLineEdit(self.centralwidget)
        self.path_train_data.setObjectName(u"path_train_data")

        self.input_path.addWidget(self.path_train_data)

        self.path_validation_data = QLineEdit(self.centralwidget)
        self.path_validation_data.setObjectName(u"path_validation_data")

        self.input_path.addWidget(self.path_validation_data)

        self.horizontalLayout.addLayout(self.input_path)

        self.browse_buttons = QVBoxLayout()
        self.browse_buttons.setObjectName(u"browse_buttons")
        self.import_train_data_btn = QPushButton(self.centralwidget)
        self.import_train_data_btn.setObjectName(u"import_train_data_btn")

        self.browse_buttons.addWidget(self.import_train_data_btn)

        self.import_validation_data_btn = QPushButton(self.centralwidget)
        self.import_validation_data_btn.setObjectName(
            u"import_validation_data_btn")

        self.browse_buttons.addWidget(self.import_validation_data_btn)

        self.horizontalLayout.addLayout(self.browse_buttons)

        self.central_widget_vertical_lyt.addLayout(self.horizontalLayout)

    def set_execute_btn_box(self):
        # Add execute buttons
        self.execute_buttons_layout = QHBoxLayout()
        self.execute_buttons_layout.setObjectName(u"execute_buttons_layout")
        self.execute_buttons_layout.setContentsMargins(-1, 0, -1, -1)
        self.train_btn = QPushButton(self.centralwidget)
        self.train_btn.setObjectName(u"train_btn")
        self.train_btn.setEnabled(False)

        self.execute_buttons_layout.addWidget(self.train_btn)

        self.to_mcu_btn = QPushButton(self.centralwidget)
        self.to_mcu_btn.setObjectName(u"to_mcu_btn")
        self.to_mcu_btn.setEnabled(False)

        self.execute_buttons_layout.addWidget(self.to_mcu_btn)

        self.validate_btn = QPushButton(self.centralwidget, "Validate")
        self.validate_btn.setObjectName(u"validate")
        self.validate_btn.setEnabled(False)

        self.execute_buttons_layout.addWidget(self.validate_btn)

        self.central_widget_vertical_lyt.addLayout(self.execute_buttons_layout)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(
            QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.actionNew.setText(
            QCoreApplication.translate("MainWindow", u"New", None))
        self.path_train_data.setPlaceholderText(
            QCoreApplication.translate("MainWindow", u"path/to/train_data",
                                       None))
        self.path_validation_data.setPlaceholderText(
            QCoreApplication.translate("MainWindow",
                                       u"path/to/validation_data", None))
        self.import_train_data_btn.setText(
            QCoreApplication.translate("MainWindow", u"Browse", None))
        self.import_validation_data_btn.setText(
            QCoreApplication.translate("MainWindow", u"Browse", None))
        self.outputText.setPlaceholderText(
            QCoreApplication.translate("MainWindow", u"OUTPUT", None))

        self.sklearn_settings_widget.setTabText(
            self.sklearn_settings_widget.indexOf(self.sklearn_settings),
            QCoreApplication.translate("MainWindow", u"Settings", None))
        self.sklearn_settings_widget.setTabText(
            self.sklearn_settings_widget.indexOf(
                self.sklearn_advanced_settings),
            QCoreApplication.translate("MainWindow", u"Advanced settings",
                                       None))
        self.ToDo.setText(QCoreApplication.translate("MainWindow", u"TF",
                                                     None))
        self.train_btn.setText(
            QCoreApplication.translate("MainWindow", u"Train", None))
        self.to_mcu_btn.setText(
            QCoreApplication.translate("MainWindow", u"To MCU", None))
        self.validate_btn.setText(
            QCoreApplication.translate("MainWindow", u"Validate", None))
        self.menuFile.setTitle(
            QCoreApplication.translate("MainWindow", u"File", None))
        self.menuEdit.setTitle(
            QCoreApplication.translate("MainWindow", u"Edit", None))

    # retranslateUi
