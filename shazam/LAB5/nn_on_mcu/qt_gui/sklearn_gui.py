from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
                            QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
                           QFontDatabase, QIcon, QKeySequence, QLinearGradient,
                           QPalette, QPainter, QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


def setup_sklearn_gui(self):
    # Setting stackedwidget widget
    self.SKLearn = QWidget()
    self.SKLearn.setObjectName(u"SKLearn")
    self.SKLearn.setMaximumSize(QSize(618, 16777215))

    # Tab widget for sklearn
    # First tab: standard settings
    # Second tab: advanced settings
    self.sklearn_settings_widget = QTabWidget(self.SKLearn)
    self.sklearn_settings_widget.setObjectName(u"tabWidget")
    self.sklearn_settings_widget.setGeometry(QRect(20, 10, 601, 501))
    self.sklearn_settings_widget.setTabsClosable(False)

    # Set first tab
    setup_standard_settings(self)
    # Set second tab
    setup_advanced_settings_list(self)

    # Add to stacked widget
    self.library_pages.addWidget(self.SKLearn)


def setup_standard_settings(self):
    # Standard Settings
    self.sklearn_settings = QWidget()
    self.sklearn_settings.setObjectName(u"sklearn_settings")
    layout = QVBoxLayout(self.sklearn_settings)

    label_solver = QLabel("Solver")
    label_solver.setMaximumHeight(40)
    layout.addWidget(label_solver)


    self.solver= QComboBox()
    self.solver.addItem("lbfgs")
    self.solver.addItem("sgd")
    self.solver.addItem("adam")
    self.solver.setObjectName(u"solver")

    layout.addWidget(self.solver)

    label_activation = QLabel("Activation function hidden layer")
    label_activation.setMaximumHeight(40)
    layout.addWidget(label_activation)


    self.activation_hidden = QComboBox()
    self.activation_hidden.addItem("identity")
    self.activation_hidden.addItem("logistic")
    self.activation_hidden.addItem("tanh")
    self.activation_hidden.addItem("relu")
    self.activation_hidden.setObjectName(u"activation_hidden")

    layout.addWidget(self.activation_hidden)

    label_hidden_size= QLabel("Amount hidden layers and sizes")
    label_hidden_size.setMaximumHeight(40)
    layout.addWidget(label_hidden_size)

    self.hidden_layer_size = QLineEdit()
    layout.addWidget(self.hidden_layer_size)

    self.sklearn_settings_widget.addTab(self.sklearn_settings, "")


def setup_advanced_settings_list(self):
    # Create tab
    self.sklearn_advanced_settings = QWidget()
    self.sklearn_advanced_settings.setObjectName(u"sklearn_advanced_settings")
    layout = QVBoxLayout(self.sklearn_advanced_settings)

    # Create settings list
    self.advanced_settings_list = QListWidget(self.sklearn_advanced_settings)

    # Create list's items
    populate_list(self)

    layout.addWidget(self.advanced_settings_list)
    self.sklearn_settings_widget.addTab(self.sklearn_advanced_settings, "")


def populate_list(self):
    # Create object
    widget = QWidget()
    label_desc = QLabel("Learning rate")
    self.learning_rate = QComboBox()
    self.learning_rate.addItem("constant")
    self.learning_rate.addItem("invscaling")
    self.learning_rate.addItem("adaptive")
    self.learning_rate.setObjectName("learning_rate")
    widget_layout = QHBoxLayout()
    widget_layout.addWidget(label_desc)
    widget_layout.addWidget(self.learning_rate)
    widget_layout.addStretch()
    widget.setLayout(widget_layout)
    # Add to list item
    self.item = QListWidgetItem()
    self.item.setSizeHint(widget.sizeHint())
    # Add to list
    self.advanced_settings_list.addItem(self.item)
    self.advanced_settings_list.setItemWidget(self.item, widget)

    # Create object
    widget = QWidget()
    label_desc = QLabel("Initial learning rate")
    self.learning_rate_init = QDoubleSpinBox()
    self.learning_rate_init.setDecimals(3)
    self.learning_rate_init.setSingleStep(0.001)
    self.learning_rate_init.setValue(0.001)
    widget_layout = QHBoxLayout()
    widget_layout.addWidget(label_desc)
    widget_layout.addWidget(self.learning_rate_init)
    widget_layout.addStretch()
    widget.setLayout(widget_layout)
    # Add to list item
    self.item = QListWidgetItem()
    self.item.setSizeHint(widget.sizeHint())
    # Add to list
    self.advanced_settings_list.addItem(self.item)
    self.advanced_settings_list.setItemWidget(self.item, widget)

    # Create object
    widget = QWidget()
    label_desc = QLabel("Shuffle")
    self.shuffle = QComboBox()
    self.shuffle.addItem("True")
    self.shuffle.addItem("False")
    self.shuffle.setObjectName("shuffle")
    widget_layout = QHBoxLayout()
    widget_layout.addWidget(label_desc)
    widget_layout.addWidget(self.shuffle)
    widget_layout.addStretch()
    widget.setLayout(widget_layout)
    # Add to list item
    self.item = QListWidgetItem()
    self.item.setSizeHint(widget.sizeHint())
    # Add to list
    self.advanced_settings_list.addItem(self.item)
    self.advanced_settings_list.setItemWidget(self.item, widget)

    # Create object
    widget = QWidget()
    label_desc = QLabel("Momentum")
    self.momentum = QDoubleSpinBox()
    self.momentum.setDecimals(1)
    self.momentum.setMaximum(1.00)
    self.momentum.setMinimum(0.00)
    self.momentum.setSingleStep(0.1)
    self.momentum.setValue(0.9)
    widget_layout = QHBoxLayout()
    widget_layout.addWidget(label_desc)
    widget_layout.addWidget(self.momentum)
    widget_layout.addStretch()
    widget.setLayout(widget_layout)
    # Add to list item
    self.item = QListWidgetItem()
    self.item.setSizeHint(widget.sizeHint())
    # Add to list
    self.advanced_settings_list.addItem(self.item)
    self.advanced_settings_list.setItemWidget(self.item, widget)

    # Create object
    widget = QWidget()
    label_desc = QLabel("Max Iteration")
    self.max_iteration = QSpinBox()
    self.max_iteration.setMaximum(999999)
    self.max_iteration.setMinimum(1)
    self.max_iteration.setValue(200)
    widget_layout = QHBoxLayout()
    widget_layout.addWidget(label_desc)
    widget_layout.addWidget(self.max_iteration)
    widget_layout.addStretch()
    widget.setLayout(widget_layout)
    # Add to list item
    self.item = QListWidgetItem()
    self.item.setSizeHint(widget.sizeHint())
    # Add to list
    self.advanced_settings_list.addItem(self.item)
    self.advanced_settings_list.setItemWidget(self.item, widget)

