import os

skl_act_func = {
    "identity": 0,
    "logistic": 3,
    "tanh": 5,
    "relu": 18  # not implemented
}


class NNOnMCU:
    NN = {}
    supported_nn = {"sklearn": ["MLPClassifier"]}

    def __init__(self, nn_lib: str = None, nn_object=None):
        if nn_lib is None:
            pass
        if nn_lib in self.supported_nn:
            if nn_lib == "sklearn":
                if type(nn_object).__name__ == 'MLPClassifier':
                    self.import_sklearn_mlp_classifier(nn_object)

    def import_sklearn_mlp_classifier(self, mlp_object):
        assert type(mlp_object).__name__ == 'MLPClassifier', \
            "Expected object of type 'MLPClassifier' from library sklearn, but got object of type: %s" % \
            type(mlp_object).__name__
        assert hasattr(mlp_object, 'coefs_'), \
            "mlp_object has no attribute coefs_, you have to train the model before passing it to this function"
        hidden_layer_sizes = list(mlp_object.hidden_layer_sizes) \
            if type(mlp_object.hidden_layer_sizes) is tuple \
            else [mlp_object.hidden_layer_sizes]
        self._import_nn(library="sklearn",
                        nn_type="MLPClassifier",
                        coefficients=mlp_object.coefs_,
                        input_size=len(mlp_object.coefs_[0]),
                        hidden_layer_sizes=hidden_layer_sizes,
                        output_size=mlp_object.n_outputs_,
						intercept_array=mlp_object.intercepts_,
                        input_layer_activation="identity",
                        hidden_layers_activations=len(hidden_layer_sizes) *
                        [mlp_object.activation],
                        output_layer_activation="logistic")
        self.NN["FANN_compatible"] = True
        # act function of sklearn input layer is always "identity"

    def _nn_to_fann_layers(self):
        fann_layers_sizes = self.NN["layers_sizes"]
        fann_layers = []
        for i, layer_size in enumerate(fann_layers_sizes):
            if i == 0:
                fann_layers += [[0, layer_size]]
            else:
                fann_layers += [[
                    fann_layers[-1][1], fann_layers[-1][1] + layer_size
                ]]
        return fann_layers

    def _nn_to_fann_neurons(self, fann_neuron_weight=0.5):
        """
        :param input_size: int, size of input layer
        :param hidden_layer_sizes: list of int, sizes of hidden layers
        :param output_size: int, size of output layer
        :param act_func_hidden_layers: list of strings, activation functions of hidden layers, member of skl_act_func
        :param act_func_out: string, activation function of output layer, member of skl_act_func
        :param fann_neuron_weight: weight of fann neuron
        :return: list of lists with fann_neurons structure, members are integers
        """
        # create layer sizes array
        fann_layers_sizes = self.NN["layers_sizes"]
        fann_act_functions = [
            skl_act_func[layer] for layer in self.NN["activation_functions"]
        ]
        # create activation function array with same size as above
        act_func = [
            skl_act_func[layer] for layer in self.NN["activation_functions"]
        ]

        # creates list of lists, where there are as many sublists as total number of neurons, each sublist represents
        # a neuron and is: [number of input neurons, activation function of neuron]
        fann_neurons_sizes = [[[fann_layers_sizes[i - 1], act_func[i]]] *
                              val if i > 0 else [[0, act_func[0]]] * val
                              for i, val in enumerate(fann_layers_sizes)]
        fann_neurons_sizes = [i for array in fann_neurons_sizes for i in array]
        f_intercept = [item for sublist in self.NN["intercept_array"] for item in sublist]

        # creates the output array from the previously initialised data
        fann_neurons = []
        for i, neuron_data in enumerate(fann_neurons_sizes):
            neuron_activation = neuron_data[1]
            neuron_size = neuron_data[0]
            if i == 0:
                #fann_neurons += [[0, neuron_size, 0.0, 0.0, neuron_activation]]
                fann_neurons += [[0, neuron_size, 0.0, neuron_activation]]
            else:
                last_neuron_end = fann_neurons[-1][1]
                fann_neurons += [[
                    last_neuron_end, last_neuron_end + neuron_size,
					#fann_neuron_weight * (last_neuron_end != 0), # activation steepness, removed because it is not required for float implementation
                    f_intercept[i - self.NN["input_size"]] if i>=self.NN["input_size"] else 0,
                    neuron_activation
                ]]
        return fann_neurons

    def _nn_to_fann_weights(self):
        return [
            i for matrix in self.NN["coefficients"]
            for i in matrix.flatten('F')
        ]

    def nn_to_fann_net(self,
                       output_file="./outputs/fann_net.h",
                       layout_file="./lib/fann_net_prototype"):
        assert self.NN["fann_compatible"] == False, \
            "It looks like the NN is not compatible with FANN. Import a compatible NN!"

        fann_neurons = self._nn_to_fann_neurons()
        fann_weights = self._nn_to_fann_weights()
        fann_layers = self._nn_to_fann_layers()

        fann_neurons_len = len(fann_neurons)
        fann_weights_len = len(fann_weights)
        fann_layer_len = len(fann_layers)

        fann_neurons_string = self._c_fai_ints(fann_neurons)
        fann_weights_string = self._c_fai_floats(fann_weights)
        fann_layer_string = self._c_fai_ints(fann_layers)

        # check if folder structure exists, if not make directory
        if not os.path.exists(os.path.dirname(output_file)):
            os.makedirs(os.path.dirname(output_file))

        with open(layout_file, "r") as fann_file:
            fann_net_prototype = fann_file.read()
        fann_net_prototype = fann_net_prototype % (
            fann_neurons_len, fann_neurons_string, fann_weights_len,
            fann_weights_string, fann_layer_len, fann_layer_string)
        with open(output_file, "w") as output:
            output.write(fann_net_prototype)
            output.close()
    def nn_to_fann_conf(self,
                       output_file="./outputs/fann_conf.h",
                       layout_file="./lib/fann_conf_prototype"):
        num_neurons = sum(self.NN["layers_sizes"])
        num_input = self.NN["input_size"]
        num_output = self.NN["output_size"]
        num_layers = len(self.NN["layers_sizes"])
        with open(layout_file, "r") as fann_file:
            fann_conf_prototype = fann_file.read()
        fann_conf_prototype = fann_conf_prototype % (
            num_neurons, num_input, num_output,
            num_layers)
        with open(output_file, "w") as output:
            output.write(fann_conf_prototype)
            output.close()

        assert self.NN["fann_compatible"] == False, \
            "It looks like the NN is not compatible with FANN. Import a compatible NN!"
    def _import_nn(self,
                   library: str,
                   nn_type: str,
                   coefficients: list,
                   input_size: int,
                   hidden_layer_sizes: list,
                   output_size: int,
				   intercept_array: list,
                   input_layer_activation: str,
                   hidden_layers_activations: list,
                   output_layer_activation: str,
                   fann_compatible: bool = False):
        self.NN = {
            "library": library,
            "nn_type": nn_type,
            "coefficients": coefficients,
            "input_size": input_size,
            "hidden_layer_sizes": hidden_layer_sizes,
            "output_size": output_size,
			"intercept_array": intercept_array,
            "input_layer_activation": input_layer_activation,
            "hidden_layers_activations": hidden_layers_activations,
            "output_layer_activation": output_layer_activation,
            "fann_compatible": fann_compatible
        }
        self.NN["layers_sizes"] = [self.NN["input_size"]] \
                                  + list(self.NN["hidden_layer_sizes"])\
                                  + [self.NN["output_size"]]
        self.NN["activation_functions"] = [self.NN["input_layer_activation"]] \
                                          + list(self.NN["hidden_layers_activations"]) \
                                          + [self.NN["output_layer_activation"]]

    @staticmethod
    def _c_fai_ints(python_list: list):
        return str(python_list) \
            .replace('[', '{') \
            .replace(']', '}')

    @staticmethod
    def _c_fai_floats(python_list: list):
        return str(["{:1.20e}".format(i) for i in python_list]) \
            .replace("'", "") \
            .replace('e-', 'e-0') \
            .replace('e+', 'e+0') \
            .replace('[', '{') \
            .replace(']', '}')


def concat_data(filenames: list, directory: str):
    content = ""
    for filename in filenames:
        with open(directory + '\\' + filename) as file:
            content += file.read()
    return content


def fann_data_to_arr(filename: str):
    file = open(filename)
    lines = file.readlines()
    labels = []
    data = []
    for line in range(0, len(lines), 2):
        data += [lines[line].replace("\n", "").strip()]
        labels += [lines[line + 1].replace("\n", "").strip()]
    data = [[int(num) for num in text.split(" ")] for text in data]
    labels = [text.split(" ").index('1') + 1 for text in labels]
    return data, labels


def dict_dataset_split_train_test(dataset, test_fraction = 0.2):
    train_data = []
    train_labels = []
    test_data = []
    test_labels = []
    for key in dataset:
        set_size = len(dataset[key])
        train_data_end = round(set_size * (1 - test_fraction))
        train_data_t = dataset[key][0: train_data_end]
        train_data += train_data_t
        train_labels += [key] * len(train_data_t)

        test_data_t = dataset[key][train_data_end:]
        test_data += test_data_t
        test_labels += [key] * len(test_data_t)
    return train_data, train_labels, test_data, test_labels
