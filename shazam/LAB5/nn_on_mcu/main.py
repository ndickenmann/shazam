# File: main.py
import sys
import numpy as np
from sklearn.neural_network import MLPClassifier
from PySide2.QtWidgets import QApplication, QMainWindow, QFileDialog, QPushButton
from qt_gui.ui_fanngui import Ui_MainWindow
import lib.helper_functions as hf
import qtmodern.styles
import qtmodern.windows
import texttable

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.ui = Ui_MainWindow(self)

        self.ui.import_train_data_btn.clicked.connect(lambda: self.path_to_file(True))
        self.ui.import_validation_data_btn.clicked.connect(lambda: self.path_to_file(False))

        # Not used now
        # self.ui.librarySelector.currentIndexChanged.connect(self.set_pages_library)

        self.nn_to_mcu = hf.NNOnMCU()
        self.data_train = []
        self.labels_train = []

        self.clf = MLPClassifier()

        self.data_validation = []
        self.labels_validation = []

        self.output_text = ""

        # Test functions
        self.ui.train_btn.clicked.connect(self.train_nn)
        self.ui.to_mcu_btn.clicked.connect(self.output_nn)
        self.ui.validate_btn.clicked.connect(self.validate_nn)

        self.user_inputs = {
            "solver": "",
            "activation_function": "",
            "hidden_layers": (),
            "learning_rate": "",
            "learning_rate_init": 0.0,
            "shuffle": True,
            "momentum": 0.0,
            "max_iteration": 0
        }
        self.fann_weights = []
        self.fann_layer = []
        self.fann_neurons = []

    def path_to_file(self, train):
        filter = "Data File (*.data)"
        filepath = QFileDialog.getOpenFileName(self, "Data File", "", filter)
        if train:
            self.ui.path_train_data.setText(filepath[0])
        else:
            self.ui.path_validation_data.setText(filepath[0])
        self.import_data(train)

    def import_data(self, train):
        print("Import data...")
        if train:
            self.data_train, self.labels_train = hf.fann_data_to_arr(str(self.ui.path_train_data.text()))
            self.set_output_text(train)
            self.ui.train_btn.setEnabled(True)
        else:
            self.data_validation, self.labels_validation = hf.fann_data_to_arr(str(self.ui.path_validation_data.text()))
            self.set_output_text(train)
            self.ui.validate_btn.setEnabled(True)

    def set_output_text(self, train):
        if train:
            datapoints = str(len(self.data_train))
            input_size = str(len(self.data_train[0]))
            output_size = str(max(self.labels_train))
            self.output_text += "Input size: " + input_size + "\n" \
                      "Output size: " + output_size + "\n" \
                      "Datapoints: " + datapoints + "\n \n"
        else:
            datapoints = str(len(self.data_validation))
            input_size = str(len(self.data_validation[0]))
            output_size = str(max(self.labels_validation))
            self.output_text += "Input size: " + input_size + "\n" \
                      "Output size: " + output_size + "\n" \
                      "Datapoints: " + datapoints + "\n \n"


        self.ui.outputText.setText(self.output_text)

    def set_pages_library(self, index):
        self.ui.libraryPages.setCurrentIndex(index)

    def train_nn(self):
        self.read_inputs()
        self.clf = MLPClassifier(
            activation=self.user_inputs["activation_function"],
            solver=self.user_inputs["solver"],
            hidden_layer_sizes=self.user_inputs["hidden_layers"],
            learning_rate=self.user_inputs["learning_rate"],
            learning_rate_init=self.user_inputs["learning_rate_init"],
            shuffle=self.user_inputs["shuffle"],
            momentum=self.user_inputs["momentum"],
            max_iter=self.user_inputs["max_iteration"])
        print(self.clf.fit(self.data_train, self.labels_train))

        self.nn_to_mcu.import_sklearn_mlp_classifier(self.clf)
        self.ui.to_mcu_btn.setEnabled(True)
        self.output_text += "Training successful \n \n"
        self.ui.outputText.setText(self.output_text)

    def validate_nn(self):
        predictions = list(self.clf.predict_proba(self.data_validation))
        predictions = ["[ %1.2f, %1.2f, %1.2f]" % (line[0], line[1], line[2]) for line in predictions] 
        print(predictions)
        self.create_table(predictions)

    def create_table(self, predictions):
        table = texttable.Texttable()
        table.set_cols_align(["c", "c", "c"])
        table.add_row(["ID", "NN Output", "Expected"])
        for i, val in enumerate(predictions):
           table.add_row([i+1, str(val), str([1 if self.labels_validation[i] == index else 0 for index in range(1, 4)])])
        self.output_text += table.draw()
        print(table.draw())
        self.ui.outputText.setText(self.output_text)


    def read_inputs(self):
        self.user_inputs["solver"] = self.ui.solver.currentText()
        self.user_inputs[
            "activation_function"] = self.ui.activation_hidden.currentText()
        self.user_inputs["hidden_layers"] = tuple(
            map(int,
                self.ui.hidden_layer_size.text().split(', ')))
        self.user_inputs["learning_rate"] = self.ui.learning_rate.currentText()
        self.user_inputs[
            "learning_rate_init"] = self.ui.learning_rate_init.value()
        self.user_inputs["shuffle"] = bool(self.ui.shuffle.currentText())
        self.user_inputs["momentum"] = self.ui.momentum.value()
        self.user_inputs["max_iteration"] = self.ui.max_iteration.value()
        print(self.user_inputs)

    def output_nn(self):
        self.nn_to_mcu.nn_to_fann_net()
        self.output_text += "Export successful \n \n"
        self.ui.outputText.setText(self.output_text)

    def test_func(self):
        print("Test")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()

    qtmodern.styles.dark(app)
    mw = qtmodern.windows.ModernWindow(window)

    mw.show()

    sys.exit(app.exec_())
