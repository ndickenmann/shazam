def arr_to_fann_data_f(labels: list, data: list):
    unique_labels = list(set(labels))
    n_labels = len(unique_labels)
    # creates identity matrix to represent each unique label in fann format
    labels_fann_format = [[int(i == j) for i in range(n_labels)] for j in range(n_labels)]
    fann_data = [[len(labels), len(data[0]), n_labels]]
    for i, label in enumerate(labels):
        fann_data += [data[i]]
        fann_data += [labels_fann_format[unique_labels.index(label)]]
    return fann_data


def fann_data_to_file(fann_data: list, output_file: str = "./outputs/fann_network.data"):
    with open(output_file, 'w') as output:
        for line in fann_data:
            output.write(("{} " * len(line) + "\n").format(*line))


def concat_data(filenames: list, directory: str):
    content = ""
    for filename in filenames:
        with open(directory + '\\' + filename) as file:
            content += file.read()
    return content


def fann_data_to_one_hot_arr(filename: str, data_start=0):
    file = open(filename)
    lines = file.readlines()
    labels = []
    data = []
    for line in range(data_start, len(lines), 2):
        data += [lines[line].replace("\n", "").strip()]
        labels += [lines[line + 1].replace("\n", "").strip()]
    data = [[float(num) for num in text.split(" ")] for text in data]
    labels = [[int(i) for i in text.split(" ")] for text in labels]
    return data, labels


def dict_dataset_split_train_test(dataset, test_fraction=0.2):
    train_data = []
    train_labels = []
    test_data = []
    test_labels = []
    for key in dataset:
        set_size = len(dataset[key])
        train_data_end = round(set_size * (1 - test_fraction))
        train_data_t = dataset[key][0: train_data_end]
        train_data += train_data_t
        train_labels += [key] * len(train_data_t)

        test_data_t = dataset[key][train_data_end:]
        test_data += test_data_t
        test_labels += [key] * len(test_data_t)
    return train_data, train_labels, test_data, test_labels
