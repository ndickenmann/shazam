import os

skl_act_func = {
    "identity": 0,
    "logistic": 3,
    "tanh": 5,
    "relu": 18  # not implemented
}


class NNOnMCU:
    NN = {}
    supported_nn = {"sklearn": ["MLPClassifier"]}

    def __init__(self, nn_object=None):
        if nn_object:
            self.import_sklearn_mlp_classifier(nn_object)

    def import_sklearn_mlp_classifier(self, mlp_object):
        """
        Initialization function, imports a sklearn MLPClassifier and saves the data relevant to the NN_on_MCU
        conversion internally using the _import_nn function
        :param mlp_object: Trained sklearn MLPClassifier, created with sklearn.neural_network.MLPClassifier
        """
        assert type(mlp_object).__name__ == 'MLPClassifier', \
            "Expected object of type 'MLPClassifier' from library sklearn, but got object of type: %s" % \
            type(mlp_object).__name__
        assert hasattr(mlp_object, 'coefs_'), \
            "mlp_object has no attribute coefs_, you have to train the model before passing it to this function"
        hidden_layer_sizes = list(mlp_object.hidden_layer_sizes) \
            if type(mlp_object.hidden_layer_sizes) is tuple \
            else [mlp_object.hidden_layer_sizes]
        self._import_nn(library="sklearn",
                        nn_type="MLPClassifier",
                        coefficients=mlp_object.coefs_,
                        input_size=len(mlp_object.coefs_[0]),
                        hidden_layer_sizes=hidden_layer_sizes,
                        output_size=mlp_object.n_outputs_,
                        input_layer_activation="identity",
                        hidden_layers_activations=len(hidden_layer_sizes) *
                                                  [mlp_object.activation],
                        output_layer_activation="logistic")
        self.NN["FANN_compatible"] = True
        # act function of sklearn input layer is always "identity"

    def _sk_to_nn_layers(self):
        return self.NN["layers_sizes"]

    def _sk_to_nn_neurons_weights(self, fann_neuron_weight=0.5):

        # create layer sizes array
        fann_layers_sizes = self.NN["layers_sizes"]
        # create activation function array with same size as above
        act_func = [
            skl_act_func[layer] for layer in self.NN["activation_functions"]
        ]

        # creates list of lists, where there are as many sublists as total number of neurons, each sublist represents
        # a neuron and is: [number of input neurons, activation function of neuron]
        fann_neurons_sizes = [[[fann_layers_sizes[i - 1], act_func[i]]] *
                              val if i > 0 else [[0, act_func[0]]] * val
                              for i, val in enumerate(fann_layers_sizes)]
        fann_neurons_sizes = [i for array in fann_neurons_sizes for i in array]

        # creates the output array from the previously initialised data
        fann_neurons = []
        fann_weight_start_stop = []
        for i, neuron_data in enumerate(fann_neurons_sizes):
            neuron_activation = neuron_data[1]
            neuron_size = neuron_data[0]
            if i == 0:
                fann_weight_start_stop += [(0, neuron_size)]
                fann_neurons += [[neuron_size, neuron_activation, 0.0]]
            else:
                last_neuron_end = fann_weight_start_stop[-1][1]
                fann_weight_start_stop += [(last_neuron_end, last_neuron_end + neuron_size)]
                fann_neurons += [[neuron_size, neuron_activation,
                                  fann_neuron_weight * (last_neuron_end != 0)]]
        # After the previous lines the final fann_neurons have been created.
        # nn_weights creation:
        weights = [i for matrix in self.NN["coefficients"]
                   for i in matrix.flatten('F')]
        weights_out = []
        for i, start_stop in enumerate(fann_weight_start_stop):
            for j in range(start_stop[0], start_stop[1]):
                weights[j] = [i, weights[j]]
        return fann_neurons, weights

    def sk_to_nn_net(self,
                     output_file="../outputs/nn_on_mcu.net",
                     layout_file="../sk_to_nn_on_mcu/sk_fann_nn_on_mcu_prototype"):
        assert self.NN["fann_compatible"] is False, \
            "It looks like the NN is not compatible with NN_ON_MCU. Import a compatible NN!"

        nn_neurons, nn_weights = self._sk_to_nn_neurons_weights()
        nn_layers = self._sk_to_nn_layers()

        nn_neurons_string = self._c_fai_ints(nn_neurons)
        nn_weights_string = self._c_fai_floats(nn_weights)
        nn_layer_string = self._c_fai_ints(nn_layers)

        # check if folder structure exists, if not make directory
        if not os.path.exists(os.path.dirname(output_file)):
            os.makedirs(os.path.dirname(output_file))

        with open(layout_file, "r") as nn_file:
            nn_net_prototype = nn_file.read()

        nn_net_prototype = nn_net_prototype % (nn_layer_string,
                                               nn_neurons_string,
                                               nn_weights_string)
        with open(output_file, "w") as output:
            output.write(nn_net_prototype)
            output.close()

    def _import_nn(self,
                   library: str,
                   nn_type: str,
                   coefficients: list,
                   input_size: int,
                   hidden_layer_sizes: list,
                   output_size: int,
                   input_layer_activation: str,
                   hidden_layers_activations: list,
                   output_layer_activation: str,
                   fann_compatible: bool = False):
        self.NN = {
            "library": library,
            "nn_type": nn_type,
            "coefficients": coefficients,
            "input_size": input_size,
            "hidden_layer_sizes": hidden_layer_sizes,
            "output_size": output_size,
            "input_layer_activation": input_layer_activation,
            "hidden_layers_activations": hidden_layers_activations,
            "output_layer_activation": output_layer_activation,
            "fann_compatible": fann_compatible
        }
        self.NN["layers_sizes"] = [self.NN["input_size"]] \
                                  + list(self.NN["hidden_layer_sizes"]) \
                                  + [self.NN["output_size"]]
        self.NN["activation_functions"] = [self.NN["input_layer_activation"]] \
                                          + list(self.NN["hidden_layers_activations"]) \
                                          + [self.NN["output_layer_activation"]]

    @staticmethod
    def _c_fai_ints(python_list: list):
        return str(python_list)\
            .replace('[', '(')\
            .replace(']', ')')\
            .replace('))', ')')\
            .replace('((', '(')

    @staticmethod
    def _c_fai_floats(python_list: list):
        return str([[i[0], "{:1.20e}".format(i[1])]
                    for i in python_list]) \
            .replace("'", "") \
            .replace('[', '(') \
            .replace(']', ')')\
            .replace('))', ')')\
            .replace('((', '(')

