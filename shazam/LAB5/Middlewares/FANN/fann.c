//Copyright (c) 2023 ETH Zurich, Tommaso Polonelli

#include <stdio.h>
#include <math.h>
#include "fann_conf.h"
#include "fann.h"
#include "fann_structs.h"
#include "fann_net.h"
#include <arm_math.h>

/* it forces global code optimization for maximum performances while running the DNN */
#pragma GCC push_options
#pragma GCC optimize ("O3")

fann_type neuron_values[NUM_NEURONS];

/* Softmax activation function optimized for the Cortex M4f */
static void fann_activation_softmax(fann_type *data, int size) {

    fann_type max = data[0];
    for (int i = 1; i < size; i++) {
        max = fann_max(max, data[i]);
    }

    fann_type sum = 0;

    for (int i = 0; i < size; i++) {
        data[i] = expf(data[i] - max);
        sum += data[i];
    }
    for (int i = 0; i < size; i++) {
        data[i] /= sum;
    }

}

fann_type *fann_run(fann_type * input)
{
	fann_type *neurons;
	unsigned int num_connections;
	fann_type *weights;
	unsigned int activation_function, layer_it, neuron_it, last_neuron;

	/* first set the input */
	arm_fill_f32(1.0f, neuron_values, NUM_NEURONS);
	arm_copy_f32(input, &neuron_values[fann_layers[0].first_neuron], NUM_INPUT);

	for(layer_it = 1; layer_it != NUM_LAYERS; ++layer_it)
	{
		last_neuron = fann_layers[layer_it].last_neuron;
		for(neuron_it = fann_layers[layer_it].first_neuron; neuron_it != last_neuron; ++neuron_it)
		{
			if(fann_neurons[neuron_it].first_connection == fann_neurons[neuron_it].last_connection)
			{
				continue;
			}

			activation_function = fann_neurons[neuron_it].activation_function;
			num_connections = fann_neurons[neuron_it].last_connection - fann_neurons[neuron_it].first_connection;
			weights = fann_weights + fann_neurons[neuron_it].first_connection;

			neurons = neuron_values + fann_layers[layer_it - 1].first_neuron;
			arm_dot_prod_f32(weights, neurons, num_connections, &neuron_values[neuron_it]);
			neuron_values[neuron_it] += fann_neurons[neuron_it].bias;

			switch(activation_function)
			{
			    case FANN_SIGMOID: //softmax
			    	if (neuron_it == (last_neuron-1)){
			    		fann_activation_softmax(neuron_values + fann_layers[layer_it].first_neuron,
			    				fann_layers[layer_it].last_neuron - fann_layers[layer_it].first_neuron);
			    	}
			        break;
			    case FANN_SIGMOID_SYMMETRIC:
			    	neuron_values[neuron_it] = tanhf(neuron_values[neuron_it]);
			        break;
			    default:
			    	// Not supported yet...
			    	return 0;
			    	break;

			}

		}
	}

	/* set the output */
	return neuron_values + fann_layers[NUM_LAYERS - 1].first_neuron;
}


#pragma GCC pop_options
