# Mini-Shazam on a Microcontroller

Minishazam has been written for and tested on the microcontroller TM32-Arm Cortex M4F. The songs have been recorded and classified on the microcontroller, thereafter the model was trained on a cpu and then the lightweight model was flashed back onto the microcontroller.

With the model trained we got accuracies up to 90%

# How to get started

1. Flash current code on the microcontroller and it will be able to classify between the pretrained songs.

2. Go into the main file and set COLLECT 1 to go into data collection mode and train on your own songs.
